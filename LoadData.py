from pyspark.sql import SparkSession
from pyspark.sql.functions import *
import json
import pandas as pd

spark = SparkSession.builder \
    .master("local[1]") \
    .appName("School") \
    .getOrCreate()

df = spark.read.option("multiline","true").json("resources/data.json")
with open('resources/data.json', 'r') as f:
    data = json.load(f)

# extract Dataframe Evaluations
df_evalutions = df.select("evaluations").withColumn('evaluations', explode('evaluations')).select("evaluations.*")
# Write the Evalution dataframe into json file
df_evalutions.write.mode("overwrite").json("resources/evaluations")

# extract Dataframe teachers using pandas
teachers = pd.DataFrame(data=data["teachers"])
#Transpose the dataframe
teachers = teachers.T
#Give a name to the index id
teachers = teachers.rename_axis('id_teacher').reset_index()
# create teachers spark dataframe from pandas dataframe
df_teachers = spark.createDataFrame(teachers)
# Write the teachers dataframe into json file
df_teachers.write.mode("overwrite").json("resources/teachers")

# extract Dataframe students using pandas
students = pd.DataFrame(data=data["student"])
#Transpose the dataframe
students = students.T
#Give a name to the index id
students = students.rename_axis('id_student').reset_index()
# create student spark dataframe from pandas dataframe
df_students = spark.createDataFrame(students)
# Write the teachers dataframe into json file
df_students.write.mode("overwrite").json("resources/students")


df_students.show(truncate=False)
df_teachers.show(truncate=False)
df_evalutions.show(truncate=False)
