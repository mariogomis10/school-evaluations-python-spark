from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder \
    .master("local[1]") \
    .appName("School") \
    .getOrCreate()



df_students = spark.read.json("resources/students")
df_evalutions = spark.read.json("resources/evaluations")
df_students.show(truncate=False)
df_evalutions.show(truncate=False)

#Left join des dataframes students et evaluation
df = df_students.join(df_evalutions, df_students.id_student == df_evalutions.student, "left")

#For each student, find every subjects on which he has been evaluated.
df = df.select(concat_ws(" ",df.name,df.surname).alias("name"), df.subject)
df = df.distinct()
df = df.groupBy("name").agg(collect_list("subject").alias("subjects"))
df.show(truncate=False)
df.write.mode("overwrite").json("resources/A01-StudentsSubjectsEvaluated")