from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder \
    .master("local[1]") \
    .appName("School") \
    .getOrCreate()

df_students = spark.read.json("resources/students")
df_evalutions = spark.read.json("resources/evaluations")


#Inner join des dataframes students et evaluation
df = df_students.join(df_evalutions, df_students.id_student == df_evalutions.student, "inner")

# Calculate the average of each class in each subject
df = df.select("class", "subject", "score")
df = df.groupBy("class", "subject").agg(round(avg("score"), 2).alias("average_score"))
df.show()
df.write.mode("overwrite").json("resources/A03-ClassesAverageOnSubjects")