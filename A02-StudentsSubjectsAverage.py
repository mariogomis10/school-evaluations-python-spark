from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder \
    .master("local[1]") \
    .appName("School") \
    .getOrCreate()



df_students = spark.read.json("resources/students")
df_evalutions = spark.read.json("resources/evaluations")

#left Join des dataframes students et evaluation
df = df_students.join(df_evalutions, df_students.id_student == df_evalutions.student, "left")


# For each student, calculate his average in each subject.
df = df.select(concat_ws(" ",df.name,df.surname).alias("name"), df.subject, df.score)
df = df.groupBy("name", "subject").agg(round(avg("score"), 2).alias("average_score"))
df.show()
df.write.mode("overwrite").json("resources/A02-StudentsSubjectsAverage")